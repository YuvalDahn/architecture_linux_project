# Magshimim machine writeup

## Stage 1

tried accessing the user and saw that the user name is `rafi` and that i dont know the password.
rebooted the machine and opend it in recovery mode, and tried to change the password using `passwd`, and it didn't work because the drive was in read only mode.
then i remounted the drive to readwrite mode using
`mount -o remount,rw /`
and changed the password using `passwd rafi` to 1234

## Stage 2

I've tried opening the `password.bits` using `myfs` and i coudn't find the password.
then I've read the file using `xxd -a password.bits | less` and saw different strings.
then I've tried reading the file using `strings password.bits` and found these passords:

- Password1
- 2468abcd
- 12345678
- passxyzw
- qwertyui

I figured that because it was only 5 passwords I could try them indivedualy.
so I've tried them all on `hrdaemon` until I found out that the right password is 2468abcd

## Stage 3

then I've traced the file syscalls using `strace` and saved it to the file `strace_out` using the command:
`echo 2468abcd | strace hrdaemon -o hr_out &> strace_out`
then I removed the unneccasery lines using grep and saved it to `files` using:
`cat strace_out | grep openat > files`
and the I removed the function name and only kept the file paths using replace in `gedit`.
than I wrote a line to write the requested data in the right format:
`cat files | while read line; do echo $line:; cat $line; printf "\n---\n"; done > results.txt`
and I added the results.txt to this repository.
